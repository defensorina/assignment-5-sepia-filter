#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "transformation/bmp.h"
#include "transformation/sepia.h"

char *get_read_error(enum read_status st)
{
    switch(st)
    {
        case READ_FILE_NOT_FOUND: return "not found file";
        case READ_INVALID_HEADER: return "invalid header";
        case READ_INVALID_BITS: return "invalid bits";
        case READ_INVALID_SIGNATURE: return "invalid signature";
        default: return "success";
    }
}
char *get_write_error(enum write_status st)
{
    switch(st)
    {
        case WRITE_ERROR: return "writting error";
        case WRITE_FILE_NOT_FOUND: return "not found file";
        default: return "success";
    }
}
int main(int argc, char **argv)
{
    if (argc != 4) {
        fprintf(stderr, "usage: image-transformer INPUT OUTPUT-C OUTPUT-ASM\n");
        return 1;
    }

    struct image img = {0};

    enum read_status read_status = open_bmp_file(argv[1], &img);
    if (read_status != READ_OK) {
        fprintf(stderr, "%s\n", get_read_error(read_status));
        free_image(&img);
        return 1;
    }
    
    struct image img_sepia_c = {0};
    struct image img_sepia_asm = {0};
    clock_t start, end;    

    start = clock();
    for (int i = 0; i < 50; i++) {
        sepia(&img, &img_sepia_c);
        if (i != 49) free_image(&img_sepia_c);
    }
    end = clock();
    printf("C:   %f s\n", (float) (end - start) / CLOCKS_PER_SEC);

    start = clock();
    for (int i = 0; i < 50; i++) {
        sepia_fast(&img, &img_sepia_asm);
        if (i != 49) free_image(&img_sepia_asm);
    }
    end = clock();
    printf("ASM: %f s\n", (float) (end - start) / CLOCKS_PER_SEC);

    free_image(&img);

    enum write_status write_status = close_bmp_file(argv[2], &img_sepia_c);
    if (write_status != WRITE_OK) {
        fprintf(stderr, "%s\n", get_write_error(write_status));
        free_image(&img_sepia_c);
        free_image(&img_sepia_asm);
        return 1;
    }

    free_image(&img_sepia_c);

    write_status = close_bmp_file(argv[3], &img_sepia_asm);
    if (write_status != WRITE_OK) {
        fprintf(stderr, "%s\n", get_write_error(write_status));
        free_image(&img_sepia_asm);
        return 1;
    }

    free_image(&img_sepia_asm);

    return 0;
}
