#include "sepia.h"

void sepia(struct image *img, struct image *result)
{
    allocate_image(result, img->width, img->height);
    if (result->data == NULL) return;
    
    for (uint64_t x = 0; x < img->width; ++x)
    {
        for (uint64_t y = 0; y < img->height; ++y)
        {
            struct pixel p = img->data[img->width * y + x];
            uint8_t r = 0.295 * p.r + 0.591 * p.g + 0.098 * p.b;
            uint8_t g = 0.225 * p.r + 0.449 * p.g + 0.075 * p.b;
            uint8_t b = 0.145 * p.r + 0.289 * p.g + 0.048* p.b;
            result->data[img->width * y + x] = (struct pixel){ b, g, r };
        }
    }
}

