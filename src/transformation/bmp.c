#include "bmp.h"
#define BMP_SIGNATURE 0x4d42

static size_t get_padding(uint64_t width) {
    return width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
}

enum read_status from_bmp(FILE *in,  struct image *img)
{
    struct bmp_header header = {0};
    size_t st = fread(&header, sizeof(struct bmp_header), 1, in);

    if (st != 1) return READ_INVALID_HEADER;
    if (header.bfType != 0x4d42) return READ_INVALID_SIGNATURE;
    if (header.biBitCount != 24) return READ_INVALID_HEADER;
    if (header.biPlanes != 1) return READ_INVALID_HEADER;
    if (header.biCompression != 0) return READ_INVALID_HEADER;
    if (fseek(in, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_HEADER;

    allocate_image(img, header.biWidth, header.biHeight);
    // size_t pd = header.biWidth % 4 == 0 ? 0 : 4 - (header.biWidth * 3) % 4;
    size_t pd = get_padding(header.biWidth);
    uint32_t count = header.biWidth * 3;
    struct pixel *pixels = img->data;

    for (size_t y = 0; y < header.biHeight - 1; ++y)
    {
        if (fread(pixels, 1, count + pd, in) != (count + pd)) return READ_INVALID_BITS;
        pixels += header.biWidth;
    }

    return fread(pixels, 1, count, in) != count ? READ_INVALID_BITS : READ_OK;
}

enum read_status open_bmp_file(char *p_file,  struct image *img)
{
    FILE *file = fopen(p_file, READ);
    if (file == NULL) return READ_FILE_NOT_FOUND;
    enum read_status st = from_bmp(file, img);
    fclose(file);
    return st;
}

enum write_status to_bmp(FILE *out,  struct image *img)
{
    // size_t pd = img->width % 4 == 0 ? 0 : 4 - (img->width * 3) % 4;
    size_t pd = get_padding(img->width);
    struct bmp_header header = 
    {
        .bfType = BMP_SIGNATURE,
        .bfileSize = sizeof(struct bmp_header) + (img->width * 3 + pd) * img->height,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;

    uint32_t count = header.biWidth * 3;
    struct pixel *pixels = img->data;

    for (uint32_t y = 0; y < header.biHeight; ++y)
    {
        if (fwrite(pixels, 1, count + pd, out) != count + pd) return WRITE_ERROR;
        pixels += header.biWidth;
    }

    return fwrite(pixels, 1, count, out) != count ? WRITE_ERROR : WRITE_OK;
}

enum write_status close_bmp_file(char *p_file,  struct image *img)
{
    FILE *file = fopen(p_file, WRITE);
    if (file == NULL) return WRITE_FILE_NOT_FOUND;
    enum write_status st = to_bmp(file, img);
    fclose(file);
    return st;
}
