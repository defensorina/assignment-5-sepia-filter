#include "image.h"

void allocate_image(struct image *img, uint64_t width, uint64_t height)
{
    img->width = width;
    img->height = height;
    size_t pd = width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
    img->data = calloc(width * height, sizeof(struct pixel) + pd);
}

void free_image(struct image *img)
{
    free(img->data);
}
