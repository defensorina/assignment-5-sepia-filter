section .text

global sepia_fast
extern allocate_image

sepia_fast:
    push rdi
    push rsi

    mov rax, rdi
    mov rdi, rsi
    mov rsi, [rax]
    mov rdx, [rax+8]
    mov rax, rsi
    imul rax, rdx
    imul rax, 3
    push rax
    call allocate_image

    pop rax
    pop rsi
    mov rsi, [rsi+16]
    pop rdi
    mov rdi, [rdi+16]
    add rax, rdi

    movaps xmm4, [.b_coeff]
    movaps xmm5, [.g_coeff]
    movaps xmm6, [.r_coeff]

.loop:
    pmovzxbd xmm0, [rdi]
    cvtdq2ps xmm0, xmm0

    movaps xmm1, xmm0
    movaps xmm2, xmm0
    movaps xmm3, xmm0

    shufps xmm1, xmm1, 00_00_00_00b
    shufps xmm2, xmm2, 01_01_01_01b
    shufps xmm3, xmm3, 10_10_10_10b

    mulps xmm1, xmm4
    mulps xmm2, xmm5
    mulps xmm3, xmm6

    addps xmm1, xmm2
    addps xmm1, xmm3

    cvtps2dq xmm1, xmm1

    pextrb byte [rsi+0], xmm1, 0
    pextrb byte [rsi+1], xmm1, 4
    pextrb byte [rsi+2], xmm1, 8

    add rdi, 3
    add rsi, 3
    cmp rax, rdi
    jne .loop

    ret
    align 16
.b_coeff:
    dd 0.048
    dd 0.075
    dd 0.098
    dd 0.0
.g_coeff:
    dd 0.289
    dd 0.449
    dd 0.591
    dd 0.0
.r_coeff:
    dd 0.145
    dd 0.225
    dd 0.295
    dd 0.0
