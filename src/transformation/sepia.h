#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

void sepia(struct image *img, struct image *result);

void sepia_fast(struct image *img, struct image *result);

#endif
